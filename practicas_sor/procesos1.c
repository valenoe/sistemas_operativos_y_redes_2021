#include<unistd.h>
#include<sys/wait.h>
#include<stdio.h>
#include<stdlib.h>

#define NFORKS 50

void saludar(int pos, int pid) {
    sleep(1);
    printf("Hijo %d: %d\n", pos, pid);

}

int main() {
    int pid, j;
    int *status;

    for (j=0; j<NFORKS; j++) {
        if ((pid = fork()) < 0 ) {
            printf ("Falla fork %d\n", pid);
            exit(0);
            }

        else if (pid==0) {
            saludar(j, getpid());
            exit(0);
            }
        else {
            waitpid(pid, status, 0);
            printf("Padre %d\n", j);
            }
    }
}  
#include<unistd.h>
#include<sys/wait.h>
#include<stdio.h>

int main() {
    int pid;
    int sumar = 100;

    pid = fork();

   if (pid==0) {
        printf("Valor de Sumar %d\n", sumar);
        sleep(20);
        printf("Soy el hijo: %d\n", getpid());   
    }else {
        //wait(NULL);
        printf("Soy el padre: %d - %d - %d\n", getpid(), pid, sumar);
    }

}  
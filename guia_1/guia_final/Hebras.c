#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <time.h>
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>


void *thread_routine(void *arg){
    int num_lines = *((int *)arg);
    printf("El hilo comienza a ejecutarse... \n");
    printf("este es el argumento que se le aso: %d \n", num_lines);
}

int main(int argc, char *argv[]){
    int i, j, argumentos[4];
    /*for(i=1; i<4; i++){
        argumentos[i] =*((int *)argv[i+1]);
        //atoi(argv[i]);
        
    }

    for(j=0;j<4;j++){
        printf("%d", argumentos[i]);
    */

    argumentos[0] = atoi(argv[1]);
    printf("%d", argumentos[0]);

    /*argumentos de entrada en el main
    argc --> "Argumento count"
        el primero siempre es el nombre con que se invoca el prgrama
        si yo necesito N0, N1, N2 y N3, entonces requiero de 5 argumentos
    argv --> "Argument vector"

    */


    clock_t t;
    t = clock();
    
    pthread_t thread1;
    int value = atoi(argv[1]);
    if( 0!= pthread_create(&thread1, NULL,  thread_routine, &value)){
        printf("ERROOOOOOOOR");
    }
    /* primer argumento: puntero a la hebra que se definio recien (&thread1)
    segundo arumento: atibutos del hilo (NULL)
    Tercer argumento: rutina que se va a hacer cuando la hebra se cree (funcion) (void *(*start_routine)(void *))
    cuarro argumento: argumentos de entrda de la funcion anterior
    */


  // pthread_join(pthread_t thread, void **retval);
    pthread_join(thread1, NULL);
    


    
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
  
    printf("\n\n------------------o----------------o----------------o---------------0--------------\n");
    printf("-----------------------------------------------------------------------------------\n");
    printf("\tEl ejercicio con hebras tomo %f segundos para ejecutarse \n", time_taken);
    printf("-----------------------------------------------------------------------------------\n");
    printf("------------------o----------------o----------------o---------------0--------------\n");
    return 0;
}
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <time.h>
#include <stdlib.h>



int definir_N(int n, int m){
    //Esta funcion se encarga de definir un numero aleatorio
    srand(time(NULL));

    
    int numero; 
    
    numero = rand()%n + m;
  
    sleep(1);
    return numero;
    
}

void lista_mensajes(int N3){
    /* Depende del valor de N3
    Muestra N3 mensajes para cada proceso hijo 
    en donde sea llamada
    */
    char *palabras[] = {"Hola mundo", "adios", "Buenos dias", "Buenas noches",
                        "¿Como estas?", "Ten un lindo dia", "Me agradas", 
                        "Recuerada tomar agua", "be happy", "abc"};
    int i, tam = sizeof(palabras)/sizeof(char *);


    srand(time(NULL));
   
    for(i = 0; i <N3; i++){
        int azar = rand()%tam;
        printf("%s\n", palabras[azar]);
    }
}


int main(int argc, char *argv[]){
   
    time_t start, end;
    start = time(NULL);
    /* N0 = tareas -> argv[1]
        N1 y N2 = segundos limite argv[2] argv[3]
        N3 = Mensajes argv[4]
    */
    int tareas, mensajes, N1 = atoi(argv[2]), N2 = atoi(argv[3]), segundos, segundos_final;
   

    printf("N1: %d y N2: %d \n", N1, N2);
    
    
    tareas =atoi(argv[1]); //definir_N(10, 1);
    printf("Tareas (procesos): %d\n", tareas);

    mensajes = atoi(argv[4]); //definir_N(10, 1);
    printf("Mensajes a presentar: %d\n", mensajes);
    
    int id, i;
    

    
    
    for(i = 0; i < tareas; i++){
       
        if((id=fork())==0){
            printf("-------------------------\nTarea: %d\n", i+1);
            printf("%s %d\n", "\tsoy el hijo y mi ide: ", getpid());
            
            segundos = definir_N(N2, N1);
            printf("\n\t\tTiempo para presentar mesajes: %d segundos\n", segundos);
    
            sleep(segundos);
            
            lista_mensajes(mensajes);

            segundos_final = definir_N(N2, N1);
            printf("\n\t\tTiempo para terminar: %d segundos\n", segundos_final);
    
            sleep(segundos_final);
    
            exit(0);
           
        }else{
            
            wait(NULL);
            printf("--------------------0----------------\n");
            printf("%s %d\n", "soy el padre y mi ide: ", getpid());

        }
        
    }
     end = time(NULL);


    printf("\n\n------------------o----------------o----------------o---------------0--------------\n");
    printf("-----------------------------------------------------------------------------------\n");
    printf("\tEl ejercicio con hebras tomo %.2f segundos para ejecutarse \n", difftime(end,start));
    printf("-----------------------------------------------------------------------------------\n");
    printf("------------------o----------------o----------------o---------------0--------------\n");
    return 0;

}
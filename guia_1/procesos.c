#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

int main(){

    int id, i;
    //printf("%d\n", id);
    for(i = 0; i<3; i++){
    id = fork();
    if (id == 0)// proceso hijo
    {
        printf("%s %d\n", "soy el hijo y mi ide: ", getpid());
        sleep(2);
    }else{ // padre
        wait(NULL);
        printf("%s %d\n", "soy el padre y mi ide: ", getpid());
    } 
    }   
    

    return 0;

}